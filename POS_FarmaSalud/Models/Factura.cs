﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_FarmaSalud.Models
{
    class Factura
    {

        
        [JsonProperty(PropertyName = "numero_documento")]
        public string NumeroDocumento { get; set; }

        [JsonProperty(PropertyName = "total_factura")]
        public Decimal TotalFactura { get; set; }

        [JsonProperty(PropertyName = "articulos")]
        public List<Articulos> Articulos { get; set; }


    }
}
