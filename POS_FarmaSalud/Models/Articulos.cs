﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_FarmaSalud.Models
{
    class Articulos
    {

        [JsonProperty(PropertyName = "id_producto")]
        public Decimal IdProducto { get; set; }

        [JsonProperty(PropertyName = "cantidad")]
        public Decimal Cantidad { get; set; }

        [JsonProperty(PropertyName = "precio")]
        public Decimal Precio { get; set; }
    }
}
