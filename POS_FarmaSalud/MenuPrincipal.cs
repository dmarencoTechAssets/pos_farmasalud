﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_FarmaSalud
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void canjearPreautorizacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PreAutorizaciones pre = new PreAutorizaciones();
                              pre.Show();
        }

        private void buscarPreautorizacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BusquedaAutorizaciones	busca = new BusquedaAutorizaciones();
									busca.Show();

        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
