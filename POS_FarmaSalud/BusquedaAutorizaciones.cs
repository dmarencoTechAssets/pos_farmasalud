﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_FarmaSalud
{
    public partial class BusquedaAutorizaciones : Form
    {
        public BusquedaAutorizaciones()
        {
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

		private void btnBuscar_Click(object sender, EventArgs e)
		{
            string sWhere = " where FORMAT(FECHA_DOCUMENTO, 'dd/MM/yyyy') between '" + dpFechaFacturacion.Text + "' and '" + dpFechaCreacion.Text + "'"  ;

            if (cboEstado.SelectedIndex > -1)
            {
                sWhere = sWhere + " and ESTADO = '" + cboEstado.Text + "'";
            }
            if (txtCodigoOsigu.Text != "")
            {
                sWhere = sWhere + " and CODIGO_OSIGU = '" + txtCodigoOsigu.Text + "'";
            }
            if (txtIVR.Text != "")
            {
                sWhere = sWhere + " and IVR = '" + txtIVR.Text + "'";
            }
            string ssql = "select CODIGO_OSIGU, IVR, DOCUMENTO, FECHA_DOCUMENTO, ESTADO, COPAGO , COASEGURO, MONTO  from autorizaciones" +
            sWhere;
            //" WHERE ESTADO = '@val1'" +
            //			   "   and FORMAT(CREADO_EL, 'dd/MM/yyyy') = '@val2'" +
            //			   "   and FORMAT(FECHA_DOCUMENTO, 'dd/MM/yyyy') = '@val3'";


            try
			{
				dtgPending.ClearSelection();
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FarmaSalud_BD"].ToString()))
				{
					using (SqlCommand comm = new SqlCommand(ssql, conn))
					{
						comm.CommandType = CommandType.Text;
						using (SqlDataAdapter sda = new SqlDataAdapter(comm))
						{
							using (DataTable dt = new DataTable())
							{
								sda.Fill(dt);
                                //dtgPending.DataSource = dt;
                                DGVReporte.DataSource = dt;
                            }
						}
					}
				}
			}
			catch (SqlException exception)
			{
				MessageBox.Show(exception.Message);
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private void label4_Click(object sender, EventArgs e)
		{

		}
	}
}
