﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using OsiguSDK.Core.Config;
using IConfigurations = OsiguSDK.Core.Config.IConfiguration;
using OsiguSDK.Providers.Clients.v1;
using OsiguSDK.Providers.Models;
using OsiguSDK.Providers.Models.Requests.v1;
using Configuration = OsiguSDK.Core.Config.Configuration;
using POS_FarmaSalud.GetFactura;
using Newtonsoft.Json.Linq;
using POS_FarmaSalud.Models;
using Newtonsoft.Json;

namespace POS_FarmaSalud
{
    public partial class PreAutorizaciones : Form
    {
        public PreAutorizaciones()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        public IConfigurations creaConfiguracionSeguridad()
        {
            IConfigurations config = new Configuration()
            {
                BaseUrl = "https://sandbox.paycodenetwork.com",
                Slug = "bluemedical",
                ClientId = "osigu_insurers_app",
                ClientSecret = "14b19f53-3804-4fe2-a103-6aa53a3df3cc"
            };
            return config;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            string coaseguro = "0";
            decimal precio = 0;
            decimal descuento = 0;
            decimal total = 0;
            int index = 0;

            IConfiguration config = creaConfiguracionSeguridad();
            var tokenClient = new TokenClient(config);
            config.Authentication.AccessToken = tokenClient.GetToken();

            var Authclient = new AuthorizationsClient(config);
            var authResult = Authclient.GetSingleAuthorization(txtCodigoOsigu.Text.Trim().ToUpper());
            var policy = new OsiguSDK.Providers.Models.Policy.PolicyHolderInfo();
            List<OsiguSDK.Providers.Models.Authorization.Item> items = authResult.Items;

            try
            {
                if (txtCodigoOsigu.Text == null || txtCodigoOsigu.Text == "")
                {
                    MessageBox.Show("Debe ingresar un codigo de autorizacion para buscar.");
                    txtCodigoOsigu.Focus();
                    return;
                }

                if (txtPin.Text == null || txtPin.Text == "")
                {
                    MessageBox.Show("Debe ingresar el pin de la autorizacion.");
                    txtPin.Focus();
                    return;
                }
                
                policy = authResult.Policy.PolicyHolder;
                txtNombreAsegurado.Text = policy.Name.ToString();
                dpFechaNacimiento.Text = policy.DateOfBirth.ToUniversalTime().ToString();
                dpFechaAutorizacion.Text = authResult.AuthorizationDate.ToUniversalTime().ToString();
                dtgProductos.Rows.Clear();

                foreach (Authorization.Item s in items)
                {
                    descuento = ((Convert.ToDecimal(coaseguro)*(Convert.ToDecimal(precio)*s.Quantity))/100);
                    total = ((Convert.ToDecimal(precio)*s.Quantity) - descuento);
                    dtgProductos.Rows.Add(s.ProductId,s.OsiguProductId, s.Name, s.Quantity, precio, coaseguro, total);

                    if (s.ProductId == null)
                    {
                        dtgProductos.Rows[index].DefaultCellStyle.ForeColor = Color.Red;
                    }

                    index++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnValidarProductos_Click(object sender, EventArgs e)
        {
            try
            {
                IConfiguration config = creaConfiguracionSeguridad();
                var tokenClient = new TokenClient(config);
                config.Authentication.AccessToken = tokenClient.GetToken();
                var claimRequest = new ClaimsClient(config);
                var request = new CreateClaimRequest();
                List<CreateClaimRequest.Item> items = new List<CreateClaimRequest.Item>();
                var queueRequest = new QueueClient(config);
                int total = dtgProductos.Rows.Count;
                int cont = 1;
                QueueStatus queueStatus = new QueueStatus();
                request.Pin = txtPin.Text;
                CreateClaimRequest.Item item = null;
                PreAutorizaciones preautorizaciones = new PreAutorizaciones();
                DateTime localDate = DateTime.Now;


                foreach (DataGridViewRow row in dtgProductos.Rows)
                {
                    if (cont < total)
                    {
                        if (Convert.ToDecimal(row.Cells[4].Value) == 0)
                        {
                            MessageBox.Show("Por favor ingresa el precio de todos los productos.");
							return;
                        }

                        item = new CreateClaimRequest.Item();
                        item.ProductId = row.Cells[0].Value.ToString();
                        item.OsiguProductId = row.Cells[1].Value.ToString();
                        item.Quantity =  Convert.ToDecimal(row.Cells[3].Value);
                        item.Price = Convert.ToDecimal(row.Cells[4].Value);
                        items.Add(item);
                        cont++;
                    }
                }

                request.Items = items;

                string queue = claimRequest.CreateClaim(txtCodigoOsigu.Text, request);

				queueStatus = queueRequest.CheckQueueStatus(queue);

                int intentos = 0;

                while (queueStatus.Status.ToString() == "PENDING" || queueStatus.Status.ToString() == "WORKING")
                {
                    if (intentos > 5)
                    {
                        MessageBox.Show("No hemos obtenido respuesta de parte de la aseguradora, por favor intentar en un momento.");
                        return;
                    }
                    else
                    {
                        queueStatus = queueRequest.CheckQueueStatus(queue);
                    }

                    intentos++;
                }

                Claim claim = new Claim();
                int i = 0;
                Decimal coinsurance = 0;
                Decimal totalProduct = 0;
                if (queueStatus.ResourceId != null && queueStatus.Error == null)
                {
                    claim = claimRequest.GetSingleClaim(queueStatus.ResourceId);

                    cont = 1;
                    total = dtgProductos.RowCount;
                    foreach (DataGridViewRow row in dtgProductos.Rows)
                    {
                        if (cont < total)
                        {
                            foreach (Claim.Item claimItem in claim.Items)
                            {
                                if (row.Cells[1].Value.Equals(claimItem.OsiguProductId))
                                {
                                    coinsurance = Convert.ToDecimal((Convert.ToDecimal(claimItem.Quantity) * Convert.ToDecimal(claimItem.Price)) * Convert.ToDecimal(claimItem.CoInsurancePercentage))/100;
                                    totalProduct = (claimItem.Quantity * claimItem.Price) - coinsurance;
                                    row.Cells[5].Value = claimItem.CoInsurancePercentage;
                                    row.Cells[6].Value = String.Format("{0:0.00##}", totalProduct);
                                }
                            }
                            cont++;
                        }
                    }

                    /*Limpiamos*/

                    dcCopago.Text = "";
                    dcTotalAseguradora.Text = "";
                    dcTotalAsegurado.Text = "";

                    Decimal TotalAsegurado = claim.Invoice.Amount;
                    dcCopago.Text = claim.Copayment.ToString();
                    dcTotalAsegurado.Text = Convert.ToString(coinsurance + Convert.ToDecimal(dcCopago.Text));
                    dcTotalAseguradora.Text = Convert.ToString(claim.Invoice.Amount);
                    MisVariables.claim = claim;



                    MessageBox.Show("Se ha validado correctamente los productos ya puede proceder a pagar la factura.");
                            InsertaReclamo(txtCodigoOsigu.Text, Convert.ToString(claim.Id), claim.VerificationCode, txtFacturaFarmaSalud.Text, localDate, "Pendiente", claim.Copayment, claim.Invoice.Amount, localDate, "UsuarioFarmaSalud");
                            InsertaBitacora(Convert.ToString(claim.Id), "Validacion de productos.", "Validacion Exitosa.");
                    return;
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                return;
            }
        }

        private void btnPagar_Click_1(object sender, EventArgs e)
        {
            IConfiguration config = creaConfiguracionSeguridad();
            var tokenClient = new TokenClient(config);
            config.Authentication.AccessToken = tokenClient.GetToken();
            ClaimsClient claimClient = new ClaimsClient(config);
            Claim complete = new Claim();
            CreateClaimRequest request = new CreateClaimRequest();
            CompleteClaimRequest completeClaimRequest = new CompleteClaimRequest();
            Invoice invoice = new Invoice();
            var consultaFactura = new SRVFactura();
            string str = consultaFactura.GetFactura(txtFacturaFarmaSalud.Text);
            Factura factura = JsonConvert.DeserializeObject<Factura>(str);
            bool existe = true;

            try
            {
                if (txtFacturaFarmaSalud.Text == "")
                {
                    MessageBox.Show("Debes ingresar la factura de FarmaSalud para poder comparar productos.");
                    return;
                }

                /* VALIDAMOS PRODUCTOS DE FARMASALUD */

                if (MisVariables.claim.Items.Count != factura.Articulos.Count)
                {
                    MessageBox.Show("La factura contiene un total de productos diferente a los de la preautorizacion.");
                    return;
                }
                else
                {
                    foreach (Claim.Item item in MisVariables.claim.Items)
                    {
                        existe = true;
                        for (int i = 0; i < factura.Articulos.Count; i++)
                        {
                            
                            if (Convert.ToDecimal(item.ProductId) != factura.Articulos[i].IdProducto)
                            {
                                existe = false;
                            }
                            else
                            {
                                existe = true;
                                break;
                            }
                            /*
                            if (existe == true)
                            {
                                break;
                            }
                            */
                        }
                    }

                    if (existe == true)
                    {
                        if (Convert.ToDecimal(dcTotalAseguradora.Text) != factura.TotalFactura)
                        {
                            MessageBox.Show("El monto en la factura de farmasalud no coincide con el monto esperado por Osigu, por favor validar.");
                            return;
                        }

                        invoice.DocumentNumber = txtFacturaFarmaSalud.Text;
                        invoice.DocumentDate = DateTime.Now;
                        invoice.Currency = "GTQ";
                        invoice.Amount = Convert.ToDecimal(dcTotalAseguradora.Text);

                        completeClaimRequest.Invoice = invoice;

                        complete = claimClient.CompleteClaimTransaction(Convert.ToString(MisVariables.claim.Id), completeClaimRequest);

                        MessageBox.Show("La factura generada es : " + complete.Invoice.DocumentNumber + " y el IVR es : " + complete.VerificationCode);
                        ActualizaEstadoReclamo(Convert.ToString(complete.Id));
                        InsertaBitacora(Convert.ToString(complete.Id), "Finalizacion de reclamo.", "Finalizacion Exitosa.");
                        return;

                    }
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
                return;
            }
        }

        public void InsertaReclamo(string codigoOsigu, string id_reclamo ,string ivr, string documento, DateTime fechaDocumento, string estado, decimal copago, decimal monto ,DateTime creadoEl, string creadoPor)
        {


            string cmdString =
                "insert into autorizaciones(codigo_osigu ,id_reclamo , ivr, documento, fecha_documento, estado, copago, monto, creado_El, creado_por) " +
                "VALUES (@val1, @val2, @val3, @val4, @val5, @val6, @val7, @val8, @val9, @val10)";
            using (
                SqlConnection conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["FarmaSalud_BD"].ToString()))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.Connection = conn;
                    comm.CommandText = cmdString;

                    comm.Parameters.AddWithValue("@val1", codigoOsigu);
                    comm.Parameters.AddWithValue("@val2", id_reclamo);
                    comm.Parameters.AddWithValue("@val3", ivr);
                    comm.Parameters.AddWithValue("@val4", documento);
                    comm.Parameters.AddWithValue("@val5", fechaDocumento);
                    comm.Parameters.AddWithValue("@val6", estado);
                    comm.Parameters.AddWithValue("@val7", copago);
                    comm.Parameters.AddWithValue("@val8", monto);
                    comm.Parameters.AddWithValue("@val9", creadoEl);
                    comm.Parameters.AddWithValue("@val10", creadoPor);
                    
                    try                                   
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (SqlException e)
                    {
                        MessageBox.Show("No se ha podido insertar el registro.");
                        conn.Close();
                    }
                }
            }
        }

        public void ActualizaEstadoReclamo(string id_reclamo)
        {


            string cmdString = "UPDATE autorizaciones SET ESTADO = 'Finalizado', documento = '"+ txtFacturaFarmaSalud.Text + "' WHERE ID_RECLAMO = " + id_reclamo;
            using (
                SqlConnection conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["FarmaSalud_BD"].ToString()))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.Connection = conn;
                    comm.CommandText = cmdString;

                    try
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (SqlException e)
                    {
                        MessageBox.Show("No se ha podido insertar el registro.");
                        conn.Close();
                    }
                }
            }
        }

        public string InsertaBitacora(string idReclamo, string proceso, string mensaje)
        {
            DateTime hoy = DateTime.Now;
            string localdate = hoy.ToString("dd/MM/yyyy");

            string cmdString =
                "INSERT INTO bitacora (ID_RECLAMO, PROCESO, MENSAJE_RECIBIDO, CREADO_EL, CREADO_POR)" + 
                " VALUES(" + idReclamo + " , '" + proceso + "' , '" + mensaje + "', " + localdate + " , 'UsuarioFarmasalud')";

            using (
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FarmaSalud_BD"].ToString()))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.Connection = conn;
                    comm.CommandText = cmdString;

                    try
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (SqlException e)
                    {
                        MessageBox.Show("No se ha podido insertar el registro.");
                        conn.Close();
                        return "Error al guardar autorizacion.";
                    }
                }
            }

            return "OK";
        }

        public string InsertaDetalleReclamo(string enviado, string recibido)
        {
            string cmdString =
                 "insert into autorizaciones(codigo_osigu ,id_reclamo , ivr, documento, fecha_documento, estado, copago, monto, creado_El, creado_por) " +
                 "VALUES (@val1, @val2, @val3, @val4, @val5, @val6, @val7, @val8, @val9, @val10)";
            using (
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FarmaSalud_BD"].ToString()))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.Connection = conn;
                    comm.CommandText = cmdString;

                    comm.Parameters.AddWithValue("@val1", enviado);
                    comm.Parameters.AddWithValue("@val2", recibido);

                    try
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (SqlException e)
                    {
                        MessageBox.Show("No se ha podido insertar el registro.");
                        conn.Close();
                        return "Error al guardar autorizacion.";
                    }
                }
            }

            return "OK";
        }

        public void calculaTotalProductos()
        {

            int total = dtgProductos.Rows.Count;
            int cont = 1;

            MisVariables.totalAseguradora = 0;
            MisVariables.totalAsegurado = 0;
            
            foreach (DataGridViewRow row in dtgProductos.Rows)
            {

                if (cont < total)
                {
                    Console.WriteLine(row.Cells[4].Value); // descuento
                    Console.WriteLine(row.Cells[6].Value); // total - descuento
                    MisVariables.totalAseguradora += Convert.ToDecimal(row.Cells[6].Value) ; // Suma todos los descuentos que serian lo que paga el asegurado.
                    MisVariables.totalAsegurado += Convert.ToDecimal(row.Cells[4].Value); // Suma los totales menos el descuento.
                    cont++;
                }
            }

            //MisVariables.totalAseguradora -= Convert.ToDecimal(dcCopago.Text); // Suma todos los descuentos que serian lo que paga el asegurado.
            MisVariables.totalAsegurado += Convert.ToDecimal(dcCopago.Text); // Suma los totales menos el descuento.

            dcTotalAsegurado.Text = MisVariables.totalAsegurado.ToString();
            dcTotalAseguradora.Text = MisVariables.totalAseguradora.ToString();
        }

        public static class MisVariables
        {
            public static string resourceId { get; set; }
            public static decimal totalProductosXCantidad { get; set; }
            public static decimal totalDescuento { get; set; }
            public static decimal totalAseguradora { get; set; }
            public static decimal totalAsegurado { get; set; }
            public static string codigoSustituto { get; set; }

            public static Claim claim { get; set; }

        }

        private void btnEliminarProducto_Click(object sender, EventArgs e)
        {

            if (dtgProductos.SelectedRows == null)
            {
                MessageBox.Show("No existen elementos para eliminar.");
                return;
            }
            else
            {
                foreach (DataGridViewRow item in dtgProductos.SelectedRows)
                {
                    dtgProductos.Rows.RemoveAt(item.Index);
                }
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void dcTotalAsegurado_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
