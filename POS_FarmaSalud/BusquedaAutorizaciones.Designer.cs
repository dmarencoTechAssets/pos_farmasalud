﻿namespace POS_FarmaSalud
{
    partial class BusquedaAutorizaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabAuthorizations = new System.Windows.Forms.TabControl();
            this.tabPending = new System.Windows.Forms.TabPage();
            this.dtgPending = new System.Windows.Forms.DataGridView();
            this.CodOsigu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IVR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Copago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Coaseguro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Monto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtIVR = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodigoOsigu = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.DGVReporte = new System.Windows.Forms.DataGridView();
            this.GrpFecFacturacion = new System.Windows.Forms.GroupBox();
            this.dpFechaCreacion = new System.Windows.Forms.DateTimePicker();
            this.dpFechaFacturacion = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabAuthorizations.SuspendLayout();
            this.tabPending.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPending)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVReporte)).BeginInit();
            this.GrpFecFacturacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabAuthorizations
            // 
            this.tabAuthorizations.Controls.Add(this.tabPending);
            this.tabAuthorizations.Location = new System.Drawing.Point(2, 155);
            this.tabAuthorizations.Name = "tabAuthorizations";
            this.tabAuthorizations.SelectedIndex = 0;
            this.tabAuthorizations.Size = new System.Drawing.Size(865, 328);
            this.tabAuthorizations.TabIndex = 0;
            // 
            // tabPending
            // 
            this.tabPending.Controls.Add(this.DGVReporte);
            this.tabPending.Controls.Add(this.dtgPending);
            this.tabPending.Location = new System.Drawing.Point(4, 22);
            this.tabPending.Name = "tabPending";
            this.tabPending.Padding = new System.Windows.Forms.Padding(3);
            this.tabPending.Size = new System.Drawing.Size(857, 302);
            this.tabPending.TabIndex = 0;
            this.tabPending.Text = "Autorizaciones BAM";
            this.tabPending.UseVisualStyleBackColor = true;
            // 
            // dtgPending
            // 
            this.dtgPending.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPending.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodOsigu,
            this.IVR,
            this.Factura,
            this.Fecha_Documento,
            this.Estado,
            this.Copago,
            this.Coaseguro,
            this.Monto});
            this.dtgPending.Location = new System.Drawing.Point(3, 6);
            this.dtgPending.Name = "dtgPending";
            this.dtgPending.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPending.Size = new System.Drawing.Size(848, 117);
            this.dtgPending.TabIndex = 0;
            this.dtgPending.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // CodOsigu
            // 
            this.CodOsigu.Frozen = true;
            this.CodOsigu.HeaderText = "Codigo Osigu";
            this.CodOsigu.Name = "CodOsigu";
            this.CodOsigu.ReadOnly = true;
            // 
            // IVR
            // 
            this.IVR.Frozen = true;
            this.IVR.HeaderText = "IVR";
            this.IVR.Name = "IVR";
            this.IVR.ReadOnly = true;
            // 
            // Factura
            // 
            this.Factura.Frozen = true;
            this.Factura.HeaderText = "Factura";
            this.Factura.Name = "Factura";
            this.Factura.ReadOnly = true;
            // 
            // Fecha_Documento
            // 
            this.Fecha_Documento.Frozen = true;
            this.Fecha_Documento.HeaderText = "Fecha Documento";
            this.Fecha_Documento.Name = "Fecha_Documento";
            this.Fecha_Documento.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.Frozen = true;
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // Copago
            // 
            this.Copago.Frozen = true;
            this.Copago.HeaderText = "Copago";
            this.Copago.Name = "Copago";
            this.Copago.ReadOnly = true;
            // 
            // Coaseguro
            // 
            this.Coaseguro.Frozen = true;
            this.Coaseguro.HeaderText = "Coaseguro";
            this.Coaseguro.Name = "Coaseguro";
            this.Coaseguro.ReadOnly = true;
            // 
            // Monto
            // 
            this.Monto.Frozen = true;
            this.Monto.HeaderText = "Monto";
            this.Monto.Name = "Monto";
            this.Monto.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GrpFecFacturacion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtIVR);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCodigoOsigu);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboEstado);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(846, 137);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(459, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "IVR";
            // 
            // txtIVR
            // 
            this.txtIVR.Location = new System.Drawing.Point(500, 52);
            this.txtIVR.Name = "txtIVR";
            this.txtIVR.Size = new System.Drawing.Size(140, 20);
            this.txtIVR.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(415, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Codigo Osigu : ";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtCodigoOsigu
            // 
            this.txtCodigoOsigu.Location = new System.Drawing.Point(500, 26);
            this.txtCodigoOsigu.Name = "txtCodigoOsigu";
            this.txtCodigoOsigu.Size = new System.Drawing.Size(140, 20);
            this.txtCodigoOsigu.TabIndex = 7;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(500, 80);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(140, 23);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Estado de autorizacion :";
            // 
            // cboEstado
            // 
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Items.AddRange(new object[] {
            "Finalizado",
            "Pendiente"});
            this.cboEstado.Location = new System.Drawing.Point(155, 26);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(200, 21);
            this.cboEstado.Sorted = true;
            this.cboEstado.TabIndex = 0;
            this.cboEstado.Text = "Selecciona un estado";
            // 
            // bindingSource1
            // 
            this.bindingSource1.CurrentChanged += new System.EventHandler(this.bindingSource1_CurrentChanged);
            // 
            // DGVReporte
            // 
            this.DGVReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVReporte.Location = new System.Drawing.Point(3, 6);
            this.DGVReporte.Name = "DGVReporte";
            this.DGVReporte.Size = new System.Drawing.Size(848, 290);
            this.DGVReporte.TabIndex = 1;
            // 
            // GrpFecFacturacion
            // 
            this.GrpFecFacturacion.Controls.Add(this.label3);
            this.GrpFecFacturacion.Controls.Add(this.label2);
            this.GrpFecFacturacion.Controls.Add(this.dpFechaCreacion);
            this.GrpFecFacturacion.Controls.Add(this.dpFechaFacturacion);
            this.GrpFecFacturacion.Location = new System.Drawing.Point(22, 51);
            this.GrpFecFacturacion.Name = "GrpFecFacturacion";
            this.GrpFecFacturacion.Size = new System.Drawing.Size(333, 84);
            this.GrpFecFacturacion.TabIndex = 13;
            this.GrpFecFacturacion.TabStop = false;
            this.GrpFecFacturacion.Text = "Fecha de facturacion : ";
            // 
            // dpFechaCreacion
            // 
            this.dpFechaCreacion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpFechaCreacion.Location = new System.Drawing.Point(53, 53);
            this.dpFechaCreacion.Name = "dpFechaCreacion";
            this.dpFechaCreacion.Size = new System.Drawing.Size(253, 20);
            this.dpFechaCreacion.TabIndex = 4;
            // 
            // dpFechaFacturacion
            // 
            this.dpFechaFacturacion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpFechaFacturacion.Location = new System.Drawing.Point(53, 27);
            this.dpFechaFacturacion.Name = "dpFechaFacturacion";
            this.dpFechaFacturacion.Size = new System.Drawing.Size(253, 20);
            this.dpFechaFacturacion.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Desde:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Hasta";
            // 
            // BusquedaAutorizaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 488);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabAuthorizations);
            this.MaximizeBox = false;
            this.Name = "BusquedaAutorizaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Busqueda de Autorizaciones";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabAuthorizations.ResumeLayout(false);
            this.tabPending.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPending)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVReporte)).EndInit();
            this.GrpFecFacturacion.ResumeLayout(false);
            this.GrpFecFacturacion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPending;
        private System.Windows.Forms.TabControl tabAuthorizations;
        private System.Windows.Forms.DataGridView dtgPending;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodOsigu;
        private System.Windows.Forms.DataGridViewTextBoxColumn IVR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Documento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Copago;
        private System.Windows.Forms.DataGridViewTextBoxColumn Coaseguro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Monto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.BindingSource bindingSource1;
		private System.Windows.Forms.Button btnBuscar;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtCodigoOsigu;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtIVR;
        private System.Windows.Forms.DataGridView DGVReporte;
        private System.Windows.Forms.GroupBox GrpFecFacturacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dpFechaCreacion;
        private System.Windows.Forms.DateTimePicker dpFechaFacturacion;
    }
}

