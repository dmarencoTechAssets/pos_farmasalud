﻿namespace POS_FarmaSalud
{
    partial class PreAutorizaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPin = new System.Windows.Forms.TextBox();
            this.txtCodigoOsigu = new System.Windows.Forms.TextBox();
            this.btnValidarProductos = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFacturaFarmaSalud = new System.Windows.Forms.TextBox();
            this.dcCopago = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dcTotalAsegurado = new System.Windows.Forms.TextBox();
            this.btnPagar = new System.Windows.Forms.Button();
            this.dcTotalAseguradora = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgProductos = new System.Windows.Forms.DataGridView();
            this.Product_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodigoOsigu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PorcentajeCoaseguro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dpFechaAutorizacion = new System.Windows.Forms.DateTimePicker();
            this.txtNombreAsegurado = new System.Windows.Forms.TextBox();
            this.dpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEliminarProducto = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductos)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtPin);
            this.groupBox1.Controls.Add(this.txtCodigoOsigu);
            this.groupBox1.Location = new System.Drawing.Point(12, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Autorizacion";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(124, 86);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(92, 22);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PIN :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Codigo Osigu :";
            // 
            // txtPin
            // 
            this.txtPin.Location = new System.Drawing.Point(124, 59);
            this.txtPin.Name = "txtPin";
            this.txtPin.Size = new System.Drawing.Size(152, 20);
            this.txtPin.TabIndex = 1;
            // 
            // txtCodigoOsigu
            // 
            this.txtCodigoOsigu.Location = new System.Drawing.Point(124, 32);
            this.txtCodigoOsigu.Name = "txtCodigoOsigu";
            this.txtCodigoOsigu.Size = new System.Drawing.Size(152, 20);
            this.txtCodigoOsigu.TabIndex = 0;
            // 
            // btnValidarProductos
            // 
            this.btnValidarProductos.Location = new System.Drawing.Point(12, 143);
            this.btnValidarProductos.Name = "btnValidarProductos";
            this.btnValidarProductos.Size = new System.Drawing.Size(145, 30);
            this.btnValidarProductos.TabIndex = 2;
            this.btnValidarProductos.Text = "Validar Productos";
            this.btnValidarProductos.UseVisualStyleBackColor = true;
            this.btnValidarProductos.Click += new System.EventHandler(this.btnValidarProductos_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtFacturaFarmaSalud);
            this.groupBox2.Controls.Add(this.dcCopago);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dcTotalAsegurado);
            this.groupBox2.Controls.Add(this.btnPagar);
            this.groupBox2.Controls.Add(this.dcTotalAseguradora);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 365);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 181);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informacion de Facturacion";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Factura FARMASALUD : ";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtFacturaFarmaSalud
            // 
            this.txtFacturaFarmaSalud.Location = new System.Drawing.Point(135, 107);
            this.txtFacturaFarmaSalud.Name = "txtFacturaFarmaSalud";
            this.txtFacturaFarmaSalud.Size = new System.Drawing.Size(115, 20);
            this.txtFacturaFarmaSalud.TabIndex = 10;
            // 
            // dcCopago
            // 
            this.dcCopago.Location = new System.Drawing.Point(135, 24);
            this.dcCopago.Name = "dcCopago";
            this.dcCopago.ReadOnly = true;
            this.dcCopago.Size = new System.Drawing.Size(115, 20);
            this.dcCopago.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(54, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Total Copago : ";
            // 
            // dcTotalAsegurado
            // 
            this.dcTotalAsegurado.Location = new System.Drawing.Point(135, 79);
            this.dcTotalAsegurado.Name = "dcTotalAsegurado";
            this.dcTotalAsegurado.ReadOnly = true;
            this.dcTotalAsegurado.Size = new System.Drawing.Size(115, 20);
            this.dcTotalAsegurado.TabIndex = 5;
            this.dcTotalAsegurado.TextChanged += new System.EventHandler(this.dcTotalAsegurado_TextChanged);
            // 
            // btnPagar
            // 
            this.btnPagar.Location = new System.Drawing.Point(135, 140);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(115, 31);
            this.btnPagar.TabIndex = 4;
            this.btnPagar.Text = "Completar Reclamo";
            this.btnPagar.UseVisualStyleBackColor = true;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click_1);
            // 
            // dcTotalAseguradora
            // 
            this.dcTotalAseguradora.Location = new System.Drawing.Point(135, 50);
            this.dcTotalAseguradora.Name = "dcTotalAseguradora";
            this.dcTotalAseguradora.ReadOnly = true;
            this.dcTotalAseguradora.Size = new System.Drawing.Size(115, 20);
            this.dcTotalAseguradora.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Total Asegurado : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Total Aseguradora : ";
            // 
            // dtgProductos
            // 
            this.dtgProductos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Product_id,
            this.CodigoOsigu,
            this.Nombre,
            this.Cantidad,
            this.Precio,
            this.PorcentajeCoaseguro,
            this.Total});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgProductos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgProductos.Location = new System.Drawing.Point(12, 179);
            this.dtgProductos.MultiSelect = false;
            this.dtgProductos.Name = "dtgProductos";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtgProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgProductos.Size = new System.Drawing.Size(702, 180);
            this.dtgProductos.TabIndex = 5;
            this.dtgProductos.Tag = "";
            // 
            // Product_id
            // 
            this.Product_id.HeaderText = "Codigo Producto";
            this.Product_id.Name = "Product_id";
            this.Product_id.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // CodigoOsigu
            // 
            this.CodigoOsigu.HeaderText = "Codigo Osigu";
            this.CodigoOsigu.Name = "CodigoOsigu";
            this.CodigoOsigu.ReadOnly = true;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre Producto";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 250;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 75;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ToolTipText = "Ingresa el precio del producto";
            this.Precio.Width = 75;
            // 
            // PorcentajeCoaseguro
            // 
            this.PorcentajeCoaseguro.HeaderText = "%Coaseguro";
            this.PorcentajeCoaseguro.Name = "PorcentajeCoaseguro";
            this.PorcentajeCoaseguro.ReadOnly = true;
            this.PorcentajeCoaseguro.Width = 75;
            // 
            // Total
            // 
            this.Total.HeaderText = "SubTotal";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 75;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dpFechaAutorizacion);
            this.groupBox3.Controls.Add(this.txtNombreAsegurado);
            this.groupBox3.Controls.Add(this.dpFechaNacimiento);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(314, 365);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(400, 181);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informacion de Asegurado";
            // 
            // dpFechaAutorizacion
            // 
            this.dpFechaAutorizacion.Enabled = false;
            this.dpFechaAutorizacion.Location = new System.Drawing.Point(140, 79);
            this.dpFechaAutorizacion.Name = "dpFechaAutorizacion";
            this.dpFechaAutorizacion.Size = new System.Drawing.Size(220, 20);
            this.dpFechaAutorizacion.TabIndex = 8;
            // 
            // txtNombreAsegurado
            // 
            this.txtNombreAsegurado.Enabled = false;
            this.txtNombreAsegurado.Location = new System.Drawing.Point(140, 29);
            this.txtNombreAsegurado.Name = "txtNombreAsegurado";
            this.txtNombreAsegurado.Size = new System.Drawing.Size(220, 20);
            this.txtNombreAsegurado.TabIndex = 7;
            // 
            // dpFechaNacimiento
            // 
            this.dpFechaNacimiento.Enabled = false;
            this.dpFechaNacimiento.Location = new System.Drawing.Point(140, 55);
            this.dpFechaNacimiento.Name = "dpFechaNacimiento";
            this.dpFechaNacimiento.Size = new System.Drawing.Size(220, 20);
            this.dpFechaNacimiento.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Fecha Autorizacion :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Fecha Nacimiento :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Nombre Asegurado :";
            // 
            // btnEliminarProducto
            // 
            this.btnEliminarProducto.Location = new System.Drawing.Point(163, 143);
            this.btnEliminarProducto.Name = "btnEliminarProducto";
            this.btnEliminarProducto.Size = new System.Drawing.Size(145, 30);
            this.btnEliminarProducto.TabIndex = 9;
            this.btnEliminarProducto.Text = "Eliminar Producto";
            this.btnEliminarProducto.UseVisualStyleBackColor = true;
            this.btnEliminarProducto.Click += new System.EventHandler(this.btnEliminarProducto_Click);
            // 
            // PreAutorizaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 548);
            this.Controls.Add(this.btnEliminarProducto);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dtgProductos);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnValidarProductos);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "PreAutorizaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Canje de Preautorizaciones";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductos)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPin;
        private System.Windows.Forms.TextBox txtCodigoOsigu;
        private System.Windows.Forms.Button btnValidarProductos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox dcTotalAsegurado;
        private System.Windows.Forms.TextBox dcTotalAseguradora;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPagar;
        public System.Windows.Forms.DataGridView dtgProductos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dpFechaAutorizacion;
        private System.Windows.Forms.TextBox txtNombreAsegurado;
        private System.Windows.Forms.DateTimePicker dpFechaNacimiento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox dcCopago;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnEliminarProducto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFacturaFarmaSalud;
		private System.Windows.Forms.DataGridViewTextBoxColumn Product_id;
		private System.Windows.Forms.DataGridViewTextBoxColumn CodigoOsigu;
		private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
		private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
		private System.Windows.Forms.DataGridViewTextBoxColumn PorcentajeCoaseguro;
		private System.Windows.Forms.DataGridViewTextBoxColumn Total;
	}
}

